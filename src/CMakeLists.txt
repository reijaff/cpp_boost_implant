
add_library(implant-lib)
target_sources(implant-lib PUBLIC implant.cpp results.cpp tasks.cpp)
target_link_libraries(implant-lib PUBLIC ${OPENSSL_LIBRARIES} ${Boost_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})


add_executable(implant)
target_sources(implant PRIVATE main.cpp)
target_link_libraries(implant PRIVATE implant-lib ${OPENSSL_LIBRARIES} ${Boost_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
