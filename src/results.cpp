#include "results.h"
Result::Result(const boost::uuids::uuid &id, std::string contents,
               const bool success)
    : id(id), contents{std::move(contents)}, success(success) {}
