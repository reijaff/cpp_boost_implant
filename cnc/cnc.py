#!/usr/bin/env python3
import uuid
import json

from flask import request, Response
from flask_restful import Resource

from flask import Flask
from flask_restful import Api


q = []
q.append('[{"type": "execute", "command": "ls /", "id": "b1ee1aa2-186c-4332-bc86-ab6d76c293e1"}]')
q.append('[{"type": "delete", "path": "/home/reijaff/git/cpp-implant/cnc/del_file", "id": "b1ee1aa2-186c-4332-bc86-ab6d76c293e2"}]')
q.append('[{"type": "list", "path": "/home/reijaff/git/cpp-implant/cnc/", "id": "b1ee1aa2-186c-4332-bc86-ab6d76c293e3"}]')
q.append('[{"type": "get", "path": "/home/reijaff/git/cpp-implant/cnc/get_file", "id": "b1ee1aa2-186c-4332-bc86-ab6d76c293e4"}]')
q.append('[{"type": "put", "path": "/home/reijaff/git/cpp-implant/cnc/put_file", "contents":"bye!", "id": "b1ee1aa2-186c-4332-bc86-ab6d76c293e4"}]')

class Results(Resource):
    # ListResults
    def get(self):
        pass

    # AddResults
    def post(self):
        # print(request.get_data())
        data = json.loads(request.get_data())
        print("json: ",json.dumps(data))
        if q:
            item = q.pop(0)
        else:
            item = '[]'
        return Response(item, mimetype="application/json", status=200)

app = Flask(__name__)
api = Api(app)

# Define the routes for each of our resources
api.add_resource(Results, '/results', endpoint='results')

if __name__ == '__main__':
    app.run(debug=True,ssl_context=('cert.pem', 'key.pem'))
