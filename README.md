# cpp_boost_implant



A simple implant showcasing modern C++. 

# Dependencies

cmake-3.1+

clang-10+

boost-1.75

openssl-lib

# Building

Perform an out-of-source build (linux):

```
mkdir build
cd build
cmake ..
make
```

(windows):

choose x64-clang-release config in VS

build 
